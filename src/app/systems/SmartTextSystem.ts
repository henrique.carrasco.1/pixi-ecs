import { System } from "@ecs/System";
import { SmartTextComponent } from "@app/components/SmartTextComponent";
import { words } from "@assets/data/words";
import { Point, Sprite } from "pixi.js";
import { instance as Game } from "@core"; // TODO: Change this
import { Updater } from "core/Updater";

export class SmartTextSystem extends System {

    currTime: number = 0;

    update(dt: number): void {
        let entities = this.getEntities(SmartTextComponent) as SmartTextComponent[];
        if (!entities) return;
        for (let e of entities) {
            if (!e.pack) return;
            if (!e.container) return;
            if (this.currTime <= e.ttl * (Updater.Instance.FPS/1000)) {
                this.currTime += dt;
            } else {
                console.log("Here 5");
                this.currTime = 0;
                let phrase = this.cosntructPhrase(e);
                console.log(phrase);
                this.createTextObjects(e, phrase);
            }
        }
    }

    initialize() {
        let entities = this.getEntities(SmartTextComponent) as SmartTextComponent[];
        console.log(entities);
        if (!entities) return;
        for (let e of entities) {
            if (!e.pack) e.pack = this.loadDefaultPackage();
        }
    }


    loadDefaultPackage() {
        return {
            name: "default",
            icons: [
                {
                    tag: ":smile:",
                    texture: PIXI.loader.resources["assets/img/icons/smile.png"].texture
                },
                {
                    tag: ":angry:",
                    texture: PIXI.loader.resources["assets/img/icons/angry.png"].texture
                },
                {
                    tag: ":love:",
                    texture: PIXI.loader.resources["assets/img/icons/love.png"].texture
                },
                {
                    tag: ":flagforbrazil:",
                    texture: PIXI.loader.resources["assets/img/icons/flag-for-brazil.png"].texture
                },
                {
                    tag: ":alarmclock:",
                    texture: PIXI.loader.resources["assets/img/icons/alarm-clock.png"].texture
                },
                {
                    tag: ":fistedhand:",
                    texture: PIXI.loader.resources["assets/img/icons/fisted-hand.png"].texture
                }
            ]
        }    
    }

    cosntructPhrase(stc: SmartTextComponent) {
        let text = "";
        for (let i = 0; i < 4; i++) {
            let r = Math.floor(Math.random() * 10);
            if (r % 2 == 0) {
                let length = stc.pack.icons.length;
                let packRnd = Math.floor(Math.random() * length);
                text += " " + stc.pack.icons[packRnd].tag;
                continue;
            }
            let length = words.length;
            let w1Rnd = Math.floor(Math.random() * length);
            length = words[w1Rnd].length;
            let w2Rnd = Math.floor(Math.random() * length);
            text += " " + words[w1Rnd][w2Rnd];  
        }
        return text;
    }

    private createTextObjects(e: SmartTextComponent, text: string, options?: TextOptions): any {
        e.container.removeChildren();
        var reg = /\:+[+a-z]+\:/g
        var tags = text.match(reg);
        var texts = text.split(reg);
        var position = new Point(-200, 0);
        e.textObjects = [];
        e.spritesObjects = [];
        var offset = 5;
        let color: string | number = '#' + Math.floor(Math.random() * 16777215).toString(16);
        let size = 36;
        if (options) {
            color = options.color;
            size = options.fontSize;
        }
        for (var i = 0; i < texts.length; i++) {
            if (texts[i].length > 0) {

                var obj = this.createText(texts[i], position, size, color);
                e.textObjects.push(obj);
                position = new Point(position.x + obj.width + offset, position.y);
                e.container.addChild(obj);
            }
            if (!tags || tags.length <= i) continue;
            var sprite = this.createSprite(e,tags[i], position);
            sprite.width = size;
            sprite.height = size;
            position = new Point(position.x + sprite.width + offset, position.y);
            e.spritesObjects.push(sprite);
            e.container.addChild(sprite);
            console.log("Adding child");
        }
        Game.app.stage = e.container;
    }

    private createText(text: string, position: Point,size: number, color?: number | string) {
        var obj = new PIXI.Text(text, { fontFamily: 'Arial', fontSize: size, fill: color });
        obj.position = position;
        console.log(obj.text);
        return obj;
    }

    private createSprite(e: SmartTextComponent,tag: string, position: Point) {
        var texture = e.pack.icons.find(x => { return x.tag === tag; }).texture;
        var sprite = new Sprite(texture);
        sprite.position = position;
        console.log(sprite)
        return sprite;
    }

}

export type TextOptions = {
    fontSize: number;
    color: number | string;
}