import { Scene } from "core/Scene";
import { SmartTextComponent } from "@app/components/SmartTextComponent";
import { EntityManager } from "@ecs/EntityManager";
import { instance as Game } from "@core";

export class SmartTextScene extends Scene {

    stc: SmartTextComponent;
    constructor() {
        super();
        this.name = "SmartTextScene";
        // this.content = [
        //     "assets/img/icons/angry.png",
        //     "assets/img/icons/smile.png",
        //     "assets/img/icons/love.png",
        //     "assets/img/icons/flag-for-brazil.png",
        //     "assets/img/icons/fisted-hand.png",
        //     "assets/img/icons/alarm-clock.png",
        // ]
    }


    setup()
    {
        this.stc = new SmartTextComponent();
        console.log("Adding Entity")
        this.stc.ttl = 2000;
        this.stc.textObjects = [];
        this.stc.spritesObjects = [];
        this.stc.container = new PIXI.Container();
        this.stc.container.name = "SmartTextComponent";
        this.addChild(this.stc.container);
        console.log(this.stc.container.width);
        console.log(Game.app.screen.width);
        this.stc.container.x = (Game.app.screen.width -this.stc.container.width) / 2;
        this.stc.container.y = (Game.app.screen.height -this.stc.container.height) / 2;
        this.stc.container.pivot.x =this.stc.container.width / 2;
        this.stc.container.pivot.y =this.stc.container.height / 2;
        EntityManager.Instance.addEntity(this.stc);
    }
    
    unload() {
        
    }


}