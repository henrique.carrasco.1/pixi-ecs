import { Text, Sprite } from "pixi.js";
import { IComponent } from "@ecs/interfaces/IComponent";
import { IconPack } from "./iconizer";
export class SmartTextComponent implements IComponent {
    textObjects: Text[];
    spritesObjects: Sprite[];
    pack: IconPack;
    container: PIXI.Container;
    ttl: number;
}
