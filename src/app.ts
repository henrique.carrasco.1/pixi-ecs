import { Application, loader, Text } from 'pixi.js';
import "pixi-layers";
import { SmartTextScene } from '@app/scenes/IconizerScene';
import { EntityManager } from '@ecs/EntityManager';
import { SmartTextSystem } from '@app/systems/SmartTextSystem';
import { Updater } from 'core/Updater';
// import { IconStage } from '@app/stages/IconStage';
// import { CardStage } from "@app/stages/cardStage";
class Game {
  app: Application;
  fpsText: Text;
  constructor() {
    // instantiate app
    this.app = new Application({
      autoResize: true,
      backgroundColor: 0x1099bb,
      sharedTicker: true,
      resolution: devicePixelRatio
    });
    document.body.appendChild(this.app.view);

    window.addEventListener('resize', this.resize.bind(this));
    //ResourcesLoader.Initialize();



    loader.add("assets/img/card.png");
    loader.add("assets/img/btnIconizer.png");
    loader.add("assets/img/btnCard.png");
    loader.add("assets/img/btnParticle.png");
    loader.add("assets/img/btnBack.png");


    loader.add("assets/img/icons/angry.png");
    loader.add("assets/img/icons/smile.png");
    loader.add("assets/img/icons/love.png");
    loader.add("assets/img/icons/flag-for-brazil.png");
    loader.add("assets/img/icons/fisted-hand.png");
    loader.add("assets/img/icons/alarm-clock.png");
    loader.load(this.setup.bind(this));
    this.resize();
  }

  setup(): void {
    var stage = new SmartTextScene();
    //SceneManager.Instance.loadScene(stage);
    stage.setup();
    this.app.stage = stage;
    this.app.ticker.add(Updater.Instance.update.bind(Updater.Instance));
    EntityManager.Instance.addSystem(new SmartTextSystem());
  }


  resize() {
    this.app.renderer.resize(window.innerWidth, window.innerHeight);
  }
}

export const instance = new Game();
