export abstract class Scene extends PIXI.display.Stage {
    name: string;
    //content: string[];
    abstract setup();
    abstract unload();
}
