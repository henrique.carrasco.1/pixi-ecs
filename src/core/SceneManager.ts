import { loader, loaders } from "pixi.js";
import { Scene } from "./Scene";
// import { instance as Game } from "@core";
export class SceneManager {
    private current: Scene;
    private loader: loaders.Loader;
    private static instance: SceneManager;
    private loadedScenes: Scene[];

    constructor() {
        this.loader = loader;
        this.loadedScenes = [];
    }

    public static get Instance(): SceneManager {
        if (!this.instance) {
            this.instance = new SceneManager();
        }
        return this.instance;
    }

    public get Current() {
        return this.current;
    }

    public loadScene(scene: Scene) {
        let old = this.current;
        if (old) this.unloadScene(old);
        if (this.loadedScenes.indexOf(scene) < 0) {
            //this.loader.add(scene.content);
            this.loader.load(scene.setup.bind(scene));
        }
        this.current = scene;
    }

    private unloadScene(scene: Scene) {
        scene.unload();
    }
}
