import { Point } from "pixi.js";
import { IComponent } from "./ECS/interfaces/IComponent";

export class GameObject {
    
    position: Point;
    rotation: Point;
    private components: IComponent[];

    addComponent<IComponent>(component: IComponent) {
        this.components.push(component);
    }

    getComponent<T extends IComponent>(t: new () => T): T {
        return <T>this.components.find(x => x instanceof t);
    }
}

