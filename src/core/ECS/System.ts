import { ISystem } from "./interfaces/ISystem";
import { IComponent } from "./interfaces/IComponent";
import { EntityManager } from "./EntityManager";

export abstract class System implements ISystem {

    //stage: PIXI.display.Stage;
    initialize() {
        
    }

    constructor() {
        //this.stage = <PIXI.display.Stage>Game.app.stage;
    }
    
    getEntities<T extends IComponent>(t: new () => T) {
        
        return EntityManager.Instance.getEntities(t);
    }
    abstract update(dt: number): void;
}