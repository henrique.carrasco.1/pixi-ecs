export interface ISystem {
    update(dt: number): void;
}
