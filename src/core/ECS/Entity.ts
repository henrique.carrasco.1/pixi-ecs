import { IComponent } from "./interfaces/IComponent";
export class Entity<T extends IComponent> {
    id: number;
    type: any;
    component: T;
    constructor(id: number, component: T) {
        this.id = id;
        this.component = component;
        this.type = typeof(component);
    }
}
