import { System } from "./System";
import { Updater } from "../Updater";
import { IComponent } from "./interfaces/IComponent";
//import { Entity } from "./Entity";
export class EntityManager {
    private systems: System[];
    private static instance: EntityManager;
    private entities: IComponent[];
    constructor() {
        this.systems = [];
        this.entities = [];
        Updater.Instance.registerUpdate(this.update.bind(this));
    }
    public static get Instance() {
        if (!this.instance) {
            this.instance = new EntityManager();
        }
        return this.instance;
    }
    addSystem(system: System) {
        this.systems.push(system);
        system.initialize();
    }
    removeSystem(system: System) {
        this.systems.splice(this.systems.indexOf(system));
    }

    getSystem<T extends System>(system: new () => T): T {
        return <T>this.systems.find(x => x instanceof system);
    }

    update(dt: number) {
        if (this.systems) {
            
            this.systems.forEach(x => x.update(dt));
        }
    }

    addEntity(object: IComponent) {
        //console.log(object.length);
        //if (object.length <= 0) return null;
        //var lenght = this.entities.length;
        //var entity = new Entity(lenght, object);
        this.entities.push(object);
        return object;
    }
    getEntities<T extends IComponent>(t: { new(): T }) {
        let z: any = new t();
        return this.entities.filter(x => { return typeof(x) === typeof(z);});
    }
    
}
