import { instance as Game } from "@core";

export class Updater {

    updaters: Function[]
    lateUpdaters: Function[]
    FPS: number;
    deltaTime: number;
    private static instance: Updater;
    constructor() {
        this.updaters = [];
        this.lateUpdaters = [];
        
    }

    public static get Instance(): Updater {
        if (!this.instance) {
            this.instance = new Updater();
        }
        return this.instance;
    }


    registerUpdate(updater: (dt: number) => void) {
        this.updaters.push(updater);
        
    }

    registerLateUpdate(updater: (dt: number) => void) {
        this.lateUpdaters.push(updater);
        
    }

    unregisterUpdate(updater: (dt: number) => {}) {
        this.updaters.splice(this.updaters.indexOf(updater));
    }
    unregisterLateUpdate(updater: (dt: number) => {}) {
        this.lateUpdaters.splice(this.lateUpdaters.indexOf(updater));
    }

    update(dt: number) {
        this.deltaTime = dt;
        this.FPS = Game.app.ticker.FPS;
        this.updaters.forEach(x => x(this.deltaTime));
        this.lateUpdaters.forEach(x => x(this.deltaTime));
    }
}